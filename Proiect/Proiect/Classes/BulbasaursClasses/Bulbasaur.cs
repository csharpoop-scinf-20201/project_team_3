﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.BulbasaursClasses
{
    class Bulbasaur : Pokemon, IOvergrow
    {
        public Bulbasaur() : base()
        {
            Name = "Bulbasaur";
            HP = 45;
            Attack = 49;
            Defense = 49;
            SpecialAttack = 65;
            SpecialDefense = 65;
            Speed = 45;
            Height = 61;
            Weight = 7;
            Category = "Seed";
            Abilities = "Overgrow";
            Gender = "male";
            Type = new List<string>(){ "Grass", "Poison" };
            Weaknesses = new List<string>() { "Fire", "Psychic", "Flying", "Ice" };
        }

        public Bulbasaur(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight, 
            string Category, string Abilities, string Gender, 
            List<string> Type, List<string> Weaknesses) : base(Name, 
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }
        public override string Eat()
        {
            return "Your bulbasaur is eating... wait a minute";
        }

        //Powers up its attack and special attack when hp is low
        public void Overgrow()
        {
            if (HP < 10)
            {
                Attack += 10;
                SpecialAttack += 7;
            }
        }

        public override void PrintImage()
        {
            string print = File.ReadAllText("bulbasaur.txt");
            Console.WriteLine(print);
        }

        public override string Speak()
        {
            return "bulba";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Ivysaur();
            Console.WriteLine($"{Name} has evolved to Ivysaur");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            Console.WriteLine($"{Name} can't devolve anymore");
        }
    }
}

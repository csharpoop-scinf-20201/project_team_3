﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.BulbasaursClasses
{
    class MegaVenusaur : Venusaur
    {
        public MegaVenusaur() : base()
        {
            Name = "MegaVenusaur";
            HP = 80;
            Attack = 100;
            Defense = 123;
            SpecialAttack = 122;
            SpecialDefense = 120;
            Speed = 80;
            Height = 240;
            Weight = 155;
            Category = "Seed";
            Abilities = "Overgrow";
            Gender = "male";
            Type = new List<string>() { "Grass", "Poison" };
            Weaknesses = new List<string>() { "Fire", "Psychic", "Flying", "Ice" };
        }

        public MegaVenusaur(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }
        public override string Eat()
        {
            return "Your mega venusaur is devouring everything!";
        }

        public override void PrintImage()
        {
            string print = System.IO.File.ReadAllText("megavenusaur.txt");
            Console.WriteLine(print);
        }

        public override string Speak()
        {
            return "MEGASAURRRRRR!";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new GigantamaxVenusaur();
            Console.WriteLine($"{Name} has evolved to GigantamaxVenusaur");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Venusaur();
            Console.WriteLine($"{Name} has regressed to Venusaur");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.BulbasaursClasses
{
    class GigantamaxVenusaur : MegaVenusaur
    {
        public GigantamaxVenusaur() : base()
        {
            Name = "GigantamaxVenusaur";
            HP = 90;
            Attack = 120;
            Defense = 143;
            SpecialAttack = 142;
            SpecialDefense = 140;
            Speed = 80;
            Height = 2400;
            Weight = 1550;
            Category = "Seed";
            Abilities = "Overgrow";
            Gender = "male";
            Type = new List<string>() { "Grass", "Poison" };
            Weaknesses = new List<string>() { "Fire", "Psychic", "Flying", "Ice" };
        }

        public GigantamaxVenusaur(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return "Your gigantamax venusaur is eating everything!";
        }

        public override void PrintImage()
        {
            string print = System.IO.File.ReadAllText("gigantamaxvenusaur.txt");
            Console.WriteLine(print);
        }

        public override string Speak()
        {
            return "GIGANTASAURRRR!!!";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            Console.WriteLine($"{Name} can't evolve anymore");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new MegaVenusaur();
            Console.WriteLine($"{Name} has regressed to MegaVenusaur");
        }
    }
}

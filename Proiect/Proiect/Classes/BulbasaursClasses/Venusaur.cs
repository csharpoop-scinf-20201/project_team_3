﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.BulbasaursClasses
{
    class Venusaur : Ivysaur
    {
        public Venusaur() : base()
        {
            Name = "Venusaur";
            HP = 80;
            Attack = 83;
            Defense = 83;
            SpecialAttack = 100;
            SpecialDefense = 100;
            Speed = 80;
            Height = 200;
            Weight = 100;
            Category = "Seed";
            Abilities = "Overgrow";
            Gender = "male";
            Type = new List<string>() { "Grass", "Poison" };
            Weaknesses = new List<string>() { "Fire", "Psychic", "Flying", "Ice" };
        }

        public Venusaur(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }
        public override string Eat()
        {
            return "Your venusaur is eating... wait a minute";
        }

        public override void PrintImage()
        {
            string print = System.IO.File.ReadAllText("venusaur.txt");
            Console.WriteLine(print);
        }

        public override string Speak()
        {
            return "SAUR!";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new MegaVenusaur();
            Console.WriteLine($"{Name} has evolved to MegaVenusaur");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Ivysaur();
            Console.WriteLine($"{Name} has regressed to Ivysaur");
        }
    }
}

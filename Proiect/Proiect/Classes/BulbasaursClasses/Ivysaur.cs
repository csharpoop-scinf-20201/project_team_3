﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.BulbasaursClasses
{
    class Ivysaur : Bulbasaur
    {
        public Ivysaur() : base()
        {
            Name = "Ivysaur";
            HP = 60;
            Attack = 63;
            Defense = 63;
            SpecialAttack = 80;
            SpecialDefense = 80;
            Speed = 60;
            Height = 100;
            Weight = 13;
            Category = "Seed";
            Abilities = "Overgrow";
            Gender = "male";
            Type = new List<string>() { "Grass", "Poison" };
            Weaknesses = new List<string>() { "Fire", "Psychic", "Flying", "Ice" };
        }

        public Ivysaur(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }
        public override string Eat()
        {
            return "Your ivysaur is eating... wait a minute";
        }

        public override void PrintImage()
        {
            string print = File.ReadAllText("ivysaur.txt");
            Console.WriteLine(print);
        }

        public override string Speak()
        {
            return "ivy!";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Venusaur();
            Console.WriteLine($"{Name} has evolved to Venusaur");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Bulbasaur();
            Console.WriteLine($"{Name} has regressed to Bulbasaur");
        }
    }
}

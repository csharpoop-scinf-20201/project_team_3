﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    public static class Eat
    {
        public static void Feed(List<Pokemon> pokemons)
        {
            Console.WriteLine("Feed a pokemon");
            string pokename = Console.ReadLine();
            Pokemon poke_name = pokemons.Find(p => p.Name == pokename);
            Console.WriteLine(poke_name.Eat());
        }
    }
}

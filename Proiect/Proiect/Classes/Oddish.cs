﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    class Oddish : Pokemon
    {

        // Acesta este constructorul implicit si il mosteneste pe cel din clasa de baza
        public Oddish() : base()
        {
            this.Name = "Oddish";
            this.HP = 3;
            this.Attack = 3;
            this.Defense = 4;
            this.SpecialAttack = 5;
            this.SpecialDefense = 4;
            this.Speed = 2;
            this.Height = 50.8;
            this.Weight = 5.4;
            this.Category = "Weed";
            this.Abilities = "Chlorophyll";
            this.Gender = "Male";
            this.Type = new List<string>(new string[] { "Grass", "Poison" });
            this.Weaknesses = new List<string>(new string[] { "Fire", "Ice", "Psychic", "Flying" });
        }

        // Acesta este constructorul complet si il mosteneste pe cel din clasa de baza
        public Oddish(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
            base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return "Yummyyy";
        }

        public override void PrintImage()
        {

            string text = File.ReadAllText("Oddish.txt");
            Console.WriteLine(text);
        }

        public override string Speak()
        {
            return "Oddish! Oddish!";
        }


        // Acesta este abilitatea speciala a lui Oddish. Cand este il lumina soarelui ii creste Speed cu 3
        public virtual void Chlorophyll(bool sunlight)
        {
            if (sunlight == true)
            {
                Speed += 3;
            }
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Gloom();
            Console.WriteLine($"{Name} has evolved to Gloom");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Oddish();
            Console.WriteLine("This is the base stage");
        }
    }
}

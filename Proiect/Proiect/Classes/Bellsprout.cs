﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    class Bellsprout : Bellossom
    {
        // Acesta este constructorul implicit si il mosteneste pe cel din clasa de baza
        public Bellsprout() : base()
        {
            this.Name = "Bellsprout";
            this.HP = 3;
            this.Attack = 5;
            this.Defense = 3;
            this.SpecialAttack = 5;
            this.SpecialDefense = 2;
            this.Speed = 3;
            this.Height = 71.12;
            this.Weight = 3.99;
            this.Category = "Flower";
            this.Abilities = "Chlorophyll";
            this.Gender = "Male";
            this.Type = new List<string>(new string[] { "Grass", "Poison" });
            this.Weaknesses = new List<string>(new string[] { "Fire", "Ice", "Psychic", "Flying" });
        }

        // Acesta este constructorul complet si il mosteneste pe cel din clasa de baza
        public Bellsprout(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
            base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return "Yummyyy";
        }

        public override void PrintImage()
        {
            string text = File.ReadAllText("Bellsprout.txt");
            Console.WriteLine(text);
        }

        public override string Speak()
        {
            return "Bellsprout!";
        }


        // Acesta este abilitatea speciala a lui Oddish. Cand este il lumina soarelui ii creste Speed cu 4
        public override void Chlorophyll(bool sunlight)
        {
            if (sunlight == true)
            {
                Speed += 3;
            }
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Weepinbell();
            Console.WriteLine($"{Name} has evolved to Weepinbell");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Bellossom();
            Console.WriteLine($"{Name} has regressed to Bellossom");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.PidgeyClasses
{
    class Pidgeot : Pidgeotto
    {
        public Pidgeot() : base()
        {
            Name = "Pidgeot";
            HP = 83;
            Attack = 80;
            Defense = 75;
            SpecialAttack = 70;
            SpecialDefense = 70;
            Speed = 101;
            Height = 1.5;
            Weight = 39.5;
            Category = "Bird";
            Abilities = "Keen Eye, Tangled Feet";
            Gender = "male";
            Type = new List<string>() { "Normal", "Flying" };
            Weaknesses = new List<string>() { "Electric", "Ice", "Rock" };
        }

        public Pidgeot(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) : base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {
        }

        public override string Eat()
        {
            return "Pidgeot is eating";
        }

        public override void PrintImage()
        {
            string print = File.ReadAllText("Pidgeot.txt");
            Console.WriteLine(print);
        }
        public override string Speak()
        {
            return "Pijo pijo pijo";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Pidgeot();
            Console.WriteLine($"This is the final stage");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Pidgeotto();
            Console.WriteLine($"{Name} has regressed to Pidgeotto");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.PidgeyClasses
{
    class Pidgeotto : Pidgey
    {
        public Pidgeotto() : base()
        {
            Name = "Pidgeotto";
            HP = 63;
            Attack = 60;
            Defense = 55;
            SpecialAttack = 50;
            SpecialDefense = 50;
            Speed = 71;
            Height = 1.1;
            Weight = 30;
            Category = "Bird";
            Abilities = "Keen Eye, Tangled Feet";
            Gender = "male";
            Type = new List<string>() { "Normal", "Flying" };
            Weaknesses = new List<string>() { "Electric", "Ice", "Rock" };
        }

        public Pidgeotto(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) : base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {
        }

        public override string Eat()
        {
            return "Pidgeotto is eating";
        }

        public override void PrintImage()
        {
            string print = File.ReadAllText("Pidgeotto.txt");
            Console.WriteLine(print);
        }
        public override string Speak()
        {
            return "Pijoo pijooooo pijoo";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Pidgeot();
            Console.WriteLine($"{Name} has evolved to Pidgeot");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Pidgey();
            Console.WriteLine($"{Name} has regressed to Pidgey");
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.PidgeyClasses
{
    class Pidgey : Pokemon
    {
        public Pidgey() : base()
        {
            Name = "Pidgey";
            HP = 40;
            Attack = 45;
            Defense = 40;
            SpecialAttack = 35;
            SpecialDefense = 35;
            Speed = 56;
            Height = 0.3;
            Weight = 1.8;
            Category = "Tiny Bird";
            Abilities = "Keen Eye, Tangled Feet";
            Gender = "male";
            Type = new List<string>() { "Normal", "Flying" };
            Weaknesses = new List<string>() { "Electric", "Ice", "Rock" };
        }

        public Pidgey(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) : base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {
        }

        public override string Eat()
        {
            return "Pidgey is eating";
        }

        public override void PrintImage()
        {
            string print = File.ReadAllText("Pidgey.txt");
            Console.WriteLine(print);
        }
        public override string Speak()
        {
            return "Brrrr brrr brrrrrrrrrrrrr brrr";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Pidgeotto();
            Console.WriteLine($"{Name} has evolved to Pidgeotto");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Pidgey();
            Console.WriteLine("This is the base stage");
        }

    }
}

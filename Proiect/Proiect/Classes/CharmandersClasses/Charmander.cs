﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.CharmandersClasses
{
    class Charmander : Pokemon, IBlaze
    {
        public Charmander() : base()
        {
            Name = "Charmander";
            HP = 39;
            Attack = 52;
            Defense = 43;
            SpecialAttack = 60;
            SpecialDefense = 50;
            Speed = 65;
            Height = 60;
            Weight = 8;
            Category = "Lizard";
            Abilities = "Blaze";
            Gender = "male";
            Type = new List<string>() { "Fire"};
            Weaknesses = new List<string>() { "Water", "Ground", "Rock"};
        }

        public Charmander(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        //Boost up attack and special attack
        public void Blaze()
        {
            Attack += 10;
            SpecialAttack += 5;
        }

        public override string Eat()
        {
            return "Your charmander is eating... wait a minute.";
        }

        public override void PrintImage()
        {
            string print = System.IO.File.ReadAllText("charmander.txt");
            Console.WriteLine(print);
        }

        public override string Speak()
        {
            return "char char";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Charmeleon();
            Console.WriteLine($"{Name} has evolved to Charmeleon");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            Console.WriteLine($"{Name} can't devolve anymore");
        }
    }
}

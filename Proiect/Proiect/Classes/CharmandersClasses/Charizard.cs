﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.CharmandersClasses
{
    class Charizard : Charmeleon
    {
        public Charizard() : base()
        {
            Name = "Charizard";
            HP = 78;
            Attack = 84;
            Defense = 78;
            SpecialAttack = 109;
            SpecialDefense = 85;
            Speed = 100;
            Height = 170;
            Weight = 90;
            Category = "Flame";
            Abilities = "Blaze";
            Gender = "male";
            Type = new List<string>() { "Fire", "Flying" };
            Weaknesses = new List<string>() { "Water", "Electric", "Rock" };
        }

        public Charizard(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return "Your charizard is devouring his food";
        }

        public override void PrintImage()
        {
            string print = System.IO.File.ReadAllText("charizard.txt");
            Console.WriteLine(print);
        }

        public override string Speak()
        {
            return "CHAAAAAAR";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new MegaCharizardX();
            Console.WriteLine($"{Name} has evolved to MegaCharizardX");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Charmeleon();
            Console.WriteLine($"{Name} has regressed to Charmeleon");
        }
    }
}

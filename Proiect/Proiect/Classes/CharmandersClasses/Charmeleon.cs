﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.CharmandersClasses
{
    class Charmeleon : Charmander
    {
        public Charmeleon() : base()
        {
            Name = "Charmeleon";
            HP = 58;
            Attack = 64;
            Defense = 58;
            SpecialAttack = 80;
            SpecialDefense = 65;
            Speed = 80;
            Height = 110;
            Weight = 19;
            Category = "Flame";
            Abilities = "Blaze";
            Gender = "male";
            Type = new List<string>() { "Fire" };
            Weaknesses = new List<string>() { "Water", "Ground", "Rock" };
        }

        public Charmeleon(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }
        public override string Eat()
        {
            return "Your charmeleon is eating... wait a minute";
        }

        public override void PrintImage()
        {
            string print = System.IO.File.ReadAllText("charmeleon.txt");
            Console.WriteLine(print);
        }

        public override string Speak()
        {
            return "Chaaaar";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Charizard();
            Console.WriteLine($"{Name} has evolved to Charizard");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Charmander();
            Console.WriteLine($"{Name} has regressed to Charmander");
        }
    }
}

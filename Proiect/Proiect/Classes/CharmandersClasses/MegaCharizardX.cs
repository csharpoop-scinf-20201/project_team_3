﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.CharmandersClasses
{
    class MegaCharizardX : Charizard
    {
        public MegaCharizardX() : base()
        {
            Name = "MegaCharizardX";
            HP = 78;
            Attack = 130;
            Defense = 111;
            SpecialAttack = 130;
            SpecialDefense = 85;
            Speed = 100;
            Height = 170;
            Weight = 110;
            Category = "Flame";
            Abilities = "Tough Claws";
            Gender = "male";
            Type = new List<string>() { "Fire", "Dragon" };
            Weaknesses = new List<string>() { "Ground", "Rock" };
        }

        public MegaCharizardX(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }
        public override string Eat()
        {
            return "Your mega charizard x is devouring everything!";
        }

        public override void PrintImage()
        {
            string print = System.IO.File.ReadAllText("megacharizardx.txt");
            Console.WriteLine(print);
        }

        public override string Speak()
        {
            return "MEGACHAAAAAR!";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new GigantamaxCharizard();
            Console.WriteLine($"{Name} has evolved to GigantamaxCharizard");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Charizard();
            Console.WriteLine($"{Name} has regressed to Charizard");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.CharmandersClasses
{
    class GigantamaxCharizard : MegaCharizardX
    {
        public GigantamaxCharizard() : base()
        {
            Name = "GigantamaxCharizard";
            HP = 98;
            Attack = 160;
            Defense = 130;
            SpecialAttack = 190;
            SpecialDefense = 100;
            Speed = 120;
            Height = 2800;
            Weight = 1100;
            Category = "Flame";
            Abilities = "Blaze";
            Gender = "male";
            Type = new List<string>() { "Fire", "Flying" };
            Weaknesses = new List<string>() { "Water", "Electric", "Rock" };
        }

        public GigantamaxCharizard(string Name, int HP, int Attack, int Defense, 
            int SpecialAttack, int SpecialDefense, int Speed, double Height,
            double Weight, string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }
        public override string Eat()
        {
            return "Your gigantamax charizard is devouring everything!";
        }

        public override void PrintImage()
        {
            string print = System.IO.File.ReadAllText("gigantamaxcharizard.txt");
            Console.WriteLine(print);
        }

        public override string Speak()
        {
            return "GIGACHAAAAAAR!!!!";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            Console.WriteLine($"{Name} can't evolve anymore");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new MegaCharizardX();
            Console.WriteLine($"{Name} has regressed to MegaCharizardX");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Proiect.Interfaces;

namespace Proiect.Classes
{
    class Pichu:Pokemon, IPikachu
    {
        public Pichu()
        {
            Name = "Pichu";
            HP = 20;
            Attack = 40;
            Defense = 15;
            SpecialAttack = 35;
            SpecialDefense = 35;
            Speed = 60;
            Height = 100;
            Weight = 4.4;
            Category = "Tiny Mouse";
            Abilities = "Static";
            Gender = "male";
            Type = new List<string>() { "Electric" };
            Weaknesses = new List<string>() { "Ground" };
        }
        public Pichu(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
            base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }


        public override string Eat()
        {
            return $"{Name} is eating...";
        }

        public override void PrintImage()
        {
            string s = File.ReadAllText("pichu.txt");
            Console.WriteLine(s);

        }

        public override string Speak()
        {
            return "Pichuu Pichuu!";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Pikachu();
            Console.WriteLine($"{Name} has evolved to Pikachu");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Pichu();
            Console.WriteLine("This is the base stage");
        }

        public void Static()
        {
            Defense += 20;
            SpecialDefense += 30;
        }
    }
}

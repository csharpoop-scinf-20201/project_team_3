﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    class Raichu:Pikachu
    {

        public Raichu()
        {
            Name = "Raichu";
            HP = 60;
            Attack = 90;
            Defense = 55;
            SpecialAttack = 90;
            SpecialDefense = 80;
            Speed = 110;
            Height = 207;
            Weight = 66.1;
            Category = "Mouse";
            Abilities = "Static";
            Gender = "male";
            Type = new List<string>() { "Electric" };
            Weaknesses = new List<string>() { "Ground" };
        }
        public Raichu(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
           base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return $"{Name} is eating...";
        }

        public override void PrintImage()
        {
            string s = File.ReadAllText("raichu.txt");
            Console.WriteLine(s);
        }

        public override string Speak()
        {
            return "Richuu";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Raichu();
            Console.WriteLine($"{Name} is the most evolved form");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Pikachu();
            Console.WriteLine($"{Name} has regrased to Pikachu");
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    class Pikachu : Pichu
    {
        public Pikachu()
        {
            Name = "Pikachu";
            HP = 35;
            Attack = 55;
            Defense = 40;
            SpecialAttack = 50;
            SpecialDefense = 50;
            Speed = 90;
            Height = 104;
            Weight = 13.2;
            Category = "Mouse";
            Abilities = "Static";
            Gender = "male";
            Type = new List<string>() { "Electric" };
            Weaknesses = new List<string>() { "Ground" };
        }
        public Pikachu(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) : 
            base( Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category,Abilities, Gender, Type, Weaknesses)
        {
          
        }


        public override string Eat()
        {
            return $"{Name} is eating...";
        }

        public override void PrintImage()
        {
            string s = File.ReadAllText("pikachu.txt");
            Console.WriteLine(s);

        }

        public override string Speak()
        {
            return "Pika Pika!";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Raichu();
            Console.WriteLine($"{Name} has evolved to Raichu");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Pichu();
            Console.WriteLine($"{Name} has regrased to Pichu");
        }
    }
}

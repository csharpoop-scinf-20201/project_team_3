﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Proiect.Classes
{
    class Butterfree : Pokemon,ISuperPower
    {
        public Butterfree() : base()
        {
            Name = "Butterfree";
            HP = 40;
            Attack = 30;
            Defense = 30;
            SpecialAttack = 60;
            SpecialDefense = 50;
            Speed = 50;
            Height = 70;
            Weight = 7;
            Category = "Butterfly";
            Abilities = "Compound Eyes";
            Gender = "male";
            Type = new List<string>() { "Bug", "Flying" };
            Weaknesses = new List<string>() { "Fire", "Electric", "Flying", "Ice","Rock" };
        }

        public Butterfree (string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name, 
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        

        public override string Eat()
        {
            return "Fooooooodd ";
        }

        public override void PrintImage()
        {
            string path = "butterfree.txt";
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    Console.WriteLine(s);
                }
            }
        }

        public override string Speak()
        {
            return "ButterFREEEEEE !!!!!! ";
        }

        public virtual string SuperPower()
        {
            return " Butterfree a activat SUPER-POWER";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Blastoise();
            Console.WriteLine($"{Name} has evolved to Blastoise");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Butterfree();
            Console.WriteLine("This is the base stage");
        }


    }
}

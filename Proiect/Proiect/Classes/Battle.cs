﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    public static class Battle
    {
        public static bool Fight(Pokemon pokemon, Pokemon pokemon2)
        {
            while (pokemon.HP > 0 || pokemon2.HP > 0)
            {
                pokemon.AttackFunction(pokemon2);
                Log(pokemon, pokemon2);
                if (pokemon2.HP <= 0)
                {
                    Console.WriteLine($"{pokemon.Name} wins!");
                    return true;
                }

                pokemon2.AttackFunction(pokemon);
                Log(pokemon, pokemon2);
                if (pokemon.HP <= 0)
                {
                    Console.WriteLine($"{pokemon2.Name} wins!");
                    return false;
                }

                pokemon.SpecialAttackFunction(pokemon2);
                Log(pokemon, pokemon2);
                if (pokemon2.HP <= 0)
                {
                    Console.WriteLine($"{pokemon.Name} wins!");
                    return true;
                }

                pokemon2.SpecialAttackFunction(pokemon);
                Log(pokemon, pokemon2);
                if (pokemon.HP <= 0)
                {
                    Console.WriteLine($"{pokemon2.Name} wins!");
                    return false;
                }
            }
            return false;
        }

        private static void Log(Pokemon pokemon, Pokemon pokemon2)
        {
            Console.WriteLine($"{pokemon2.Name} -> {pokemon2.HP}");
            Console.WriteLine($"{pokemon.Name} -> {pokemon.HP}");
        }
    }
}
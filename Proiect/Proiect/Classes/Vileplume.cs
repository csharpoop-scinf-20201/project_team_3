﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    class Vileplume : Gloom
    {
        // Acesta este constructorul implicit si il mosteneste pe cel din clasa de baza
        public Vileplume() : base()
        {
            this.Name = "Vileplume";
            this.HP = 5;
            this.Attack = 5;
            this.Defense = 5;
            this.SpecialAttack = 7;
            this.SpecialDefense = 6;
            this.Speed = 3;
            this.Height = 119.38;
            this.Weight = 18.59;
            this.Category = "Flower";
            this.Abilities = "Chlorophyll";
            this.Gender = "Male";
            this.Type = new List<string>(new string[] { "Grass", "Poison" });
            this.Weaknesses = new List<string>(new string[] { "Fire", "Ice", "Psychic", "Flying" });
        }

        // Acesta este constructorul complet si il mosteneste pe cel din clasa de baza
        public Vileplume(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
            base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return "Yummyyy";
        }

        public override void PrintImage()
        {
            string text = File.ReadAllText("Vileplume.txt");
            Console.WriteLine(text);
        }

        public override string Speak()
        {
            return "Vileplume!";
        }

        // Acesta este abilitatea speciala a lui Oddish. Cand este il lumina soarelui ii creste Speed cu 4
        public override void Chlorophyll(bool sunlight)
        {
            if (sunlight == true)
            {
                Speed += 5;
            }
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Bellossom();
            Console.WriteLine($"{Name} has evolved to Bellossom");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Gloom();
            Console.WriteLine($"{Name} has regressed to Gloom");
        }
    }
}

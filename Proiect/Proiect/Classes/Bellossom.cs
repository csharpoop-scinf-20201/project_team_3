﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    class Bellossom : Vileplume
    {
        // Acesta este constructorul implicit si il mosteneste pe cel din clasa de baza
        public Bellossom() : base()
        {
            this.Name = "Bellossom";
            this.HP = 5;
            this.Attack = 5;
            this.Defense = 6;
            this.SpecialAttack = 6;
            this.SpecialDefense = 6;
            this.Speed = 3;
            this.Height = 40.64;
            this.Weight = 5.8;
            this.Category = "Flower";
            this.Abilities = "Chlorophyll";
            this.Gender = "Male";
            this.Type = new List<string>(new string[] { "Grass" });
            this.Weaknesses = new List<string>(new string[] { "Fire", "Ice", "Poison", "Flying", "Bug" });
        }

        // Acesta este constructorul complet si il mosteneste pe cel din clasa de baza
        public Bellossom(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
            base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return "Yummyyy";
        }

        public override void PrintImage()
        {
            string text = File.ReadAllText("Bellossom.txt");
            Console.WriteLine(text);
        }

        public override string Speak()
        {
            return "Bellossom!";
        }


        // Acesta este abilitatea speciala a lui Oddish. Cand este il lumina soarelui ii creste Speed cu 4
        public override void Chlorophyll(bool sunlight)
        {
            if (sunlight == true)
            {
                Speed += 6;
            }
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Bellsprout();
            Console.WriteLine($"{Name} has evolved to Bellsprout");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Vileplume();
            Console.WriteLine($"{Name} has regressed to Vileplume");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Proiect.Classes
{
    class Wartortle: Squirtle
    {

        public Wartortle() : base()
        {
            Name = "Wartortle";
            HP = 40;
            Attack = 45;
            Defense = 50;
            SpecialAttack = 40;
            SpecialDefense = 50;
            Speed = 40;
            Height = 30;
            Weight = 50;
            Category = "Turtle";
            Abilities = "Torrent";
            Gender = "male";
            Type = new List<string>() { "Water" };
            Weaknesses = new List<string>() { "Grass", "Electric" };
        }

        public Wartortle(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

       

        public override string Eat()
        {
            return "Fooooooodddd";
        }

        public override void PrintImage()
        {
            string path = "wartortle.txt";
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    Console.WriteLine(s);
                }
            }
        }
        public override string Speak()
        {
            return "Wartooooooortleeeeeeeeeeeeeee!";
        }

       

        public override string SuperPower()
        {
            return " Wartortle a activat SUPER-POWER";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Wartortle();
            Console.WriteLine("This is the final stage");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Squirtle();
            Console.WriteLine($"{Name} has regressed to Squirtle");
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Proiect.Classes.Nido;

namespace Proiect.Classes
{
    class Nidorina:Nidoran
    {
        public Nidorina()
        {
            Name = "Nidorina";
            HP = 70;
            Attack = 62;
            Defense = 67;
            SpecialAttack = 55;
            SpecialDefense = 55;
            Speed = 56;
            Height = 207;
            Weight = 44.1;
            Category = "Poison Pin";
            Abilities = "Poison";
            Gender = "male";
            Type = new List<string>() { "Poison" };
            Weaknesses = new List<string>() { "Psychic", "Ground" };
        }
        public Nidorina(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
           base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }


        public override string Eat()
        {
            return $"{Name} is eating...";
        }

        public override void PrintImage()
        {
            string s = File.ReadAllText("Nidorina.txt");
            Console.WriteLine(s);
        }

        public override string Speak()
        {
            return "Nidooo";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Nidoqueen();
            Console.WriteLine($"{Name} has evolved to Nidoqueen");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Nidoran();
            Console.WriteLine($"{Name} has regrased to Nidoran");
        }

    }
}

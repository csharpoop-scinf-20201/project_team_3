﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Proiect.Interfaces;

namespace Proiect.Classes
{
    class Nidoran:Pokemon, INido
    {
        public Nidoran()
        {
            Name = "Nidoran";
            HP = 55;
            Attack = 47;
            Defense = 52;
            SpecialAttack = 40;
            SpecialDefense = 40;
            Speed = 41;
            Height = 104;
            Weight = 15.4;
            Category = "Poison Pin";
            Abilities = "Poison";
            Gender = "male";
            Type = new List<string>() { "Poison" };
            Weaknesses = new List<string>() { "Psychic", "Ground" };
        }

        public Nidoran(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
           base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }
        public override string Eat()
        {
            return $"{Name} is eating...";
        }

        public override void PrintImage()
        {
            string s = File.ReadAllText("Nidoran.txt");
            Console.WriteLine(s);
        }

        public override string Speak()
        {
            return "Nidooo";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Nidorina();
            Console.WriteLine($"{Name} has evolved to Nidorina");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Nidoran();
            Console.WriteLine("This is the base stage");
        }

        public void Poison()
        {
            SpecialAttack += 140;
        }
    }
}

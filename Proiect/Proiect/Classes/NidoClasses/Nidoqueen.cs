﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.Nido
{
    class Nidoqueen:Nidorina
    {
        public Nidoqueen()
        {
            Name = "Nidoqueen";
            HP = 90;
            Attack = 92;
            Defense = 87;
            SpecialAttack = 75;
            SpecialDefense = 85;
            Speed = 76;
            Height = 403;
            Weight = 132.3;
            Category = "Drill";
            Abilities = "Poison Point";
            Gender = "male";
            Type = new List<string>() { "Poison", "Ground" };
            Weaknesses = new List<string>() { "Psychic", "Ground", "Water","Ice" };
        }

        public Nidoqueen(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
           base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return $"{Name} is eating...";
        }

        public override void PrintImage()
        {
            string s = File.ReadAllText("Nidoqueen.txt");
            Console.WriteLine(s);
        }

        public override string Speak()
        {
            return "Nidooo";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Nidoqueen();
            Console.WriteLine($"{Name} is the most evolved form");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Nidorina();
            Console.WriteLine($"{Name} has regrased to Nidorina");
        }
    }
}

﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    class Weepinbell : Bellsprout
    {
        // Acesta este constructorul implicit si il mosteneste pe cel din clasa de baza
        public Weepinbell() : base()
        {
            this.Name = "Weepinbell";
            this.HP = 4;
            this.Attack = 5;
            this.Defense = 3;
            this.SpecialAttack = 5;
            this.SpecialDefense = 3;
            this.Speed = 4;
            this.Height = 99.06;
            this.Weight = 6.4;
            this.Category = "Flycatcher";
            this.Abilities = "Chlorophyll";
            this.Gender = "Male";
            this.Type = new List<string>(new string[] { "Grass", "Poison" });
            this.Weaknesses = new List<string>(new string[] { "Fire", "Ice", "Psychic", "Flying" });
        }

        // Acesta este constructorul complet si il mosteneste pe cel din clasa de baza
        public Weepinbell(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
            base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return "Yummyyy";
        }

        public override void PrintImage()
        {
            string text = File.ReadAllText("Weepinbell.txt");
            Console.WriteLine(text);
        }

        public override string Speak()
        {
            return "Weepinbell!";
        }

        // Acesta este abilitatea speciala a lui Oddish. Cand este il lumina soarelui ii creste Speed cu 4
        public override void Chlorophyll(bool sunlight)
        {
            if (sunlight == true)
            {
                Speed += 4;
            }
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Victreebel();
            Console.WriteLine($"{Name} has evolved to Victreebel");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Bellsprout();
            Console.WriteLine($"{Name} has regressed to Bellsprout");
        }
    }
}

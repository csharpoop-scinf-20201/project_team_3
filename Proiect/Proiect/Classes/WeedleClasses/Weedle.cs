﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Drawing;
using System.IO;

namespace Proiect.Classes.WeedleClasses
{
    public class Weedle : Pokemon
    {

        public Weedle() : base()
        {
            Name = "Weedle";
            this.HP = 40;
            this.Attack = 35;
            this.Defense = 30;
            this.SpecialAttack = 20;
            this.SpecialDefense = 20;
            this.Speed = 50;
            this.Height = 0.3;
            this.Weight = 3.2;
            this.Category = "Hairy Bug";
            this.Abilities = "Shield Dust";
            this.Gender = "female";
            this.Type = new List<string>(new string[] { "Bug", "Poison" });
            this.Weaknesses = new List<string>(new string[] { "Fire", "Psychic", "Flying", "Rock" });
        }

        public Weedle(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) : base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {
        }


        public override string Eat()
        {
            return "Weedle is eating";
        }

        public override void PrintImage()
        {
            string print = File.ReadAllText("Weedle.txt");
            Console.WriteLine(print);
        }

        public override string Speak()
        {
            return "Weeedel";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Kakuna();
            Console.WriteLine($"{Name} has evolved to Kakuna");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Weedle();
            Console.WriteLine($"{Name} this is the base stage");
        }

    }
}

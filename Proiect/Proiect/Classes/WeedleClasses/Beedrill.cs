﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.WeedleClasses
{
    class Beedrill : Kakuna
    {
        public Beedrill() : base()
        {
            Name = "Beedrill";
            HP = 65;
            Attack = 90;
            Defense = 40;
            SpecialAttack = 45;
            SpecialDefense = 80;
            Speed = 75;
            Height = 1;
            Weight = 29.5;
            Category = "Poison Bee";
            Abilities = "Swarm";
            Gender = "female";
            Type = new List<string>() { "Bug", "Poison" };
            Weaknesses = new List<string>() { "Fire", "Psychic", "Flying", "Rock" };
        }

        public Beedrill(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) : base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {
        }

        public override string Eat()
        {
            return "Beedrill is eating";
        }

        public override void PrintImage()
        {
            string print = File.ReadAllText("Beedrill.txt");
            Console.WriteLine(print);
        }
        public override string Speak()
        {
            return "Beeeezzzeeezzzeeezzzz";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Beedrill();
            Console.WriteLine("This is the final stage");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Kakuna();
            Console.WriteLine($"{Name} has regressed to Kakuna");
        }
    }
}

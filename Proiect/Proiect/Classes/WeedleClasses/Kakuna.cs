﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes.WeedleClasses
{
    class Kakuna : Weedle
    {
        public Kakuna() : base()
        {
            Name = "Kakuna";
            HP = 45;
            Attack = 25;
            Defense = 50;
            SpecialAttack = 25;
            SpecialDefense = 25;
            Speed = 35;
            Height = 0.6;
            Weight = 10;
            Category = "Cocoon";
            Abilities = "Shed Skin";
            Gender = "female";
            Type = new List<string>() { "Bug", "Poison" };
            Weaknesses = new List<string>() { "Fire", "Psychic", "Flying", "Rock" };
        }

        public Kakuna(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) : base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {
        }

        public override string Eat()
        {
            return "Kakuna is eating";
        }

        public override void PrintImage()
        {
            string print = File.ReadAllText("Kakuna.txt");
            Console.WriteLine(print);
        }
        public override string Speak()
        {
            return "Kaku";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Beedrill();
            Console.WriteLine($"{Name} has evolved to Beedrill");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Weedle();
            Console.WriteLine($"{Name} has regressed to Weedle");
        }
    }
}

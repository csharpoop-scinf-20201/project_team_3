﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Proiect.Classes;

namespace Proiect.Classes
{
    public class Pokemon_Fight
    {
        public void Arena(List<Pokemon> pokemons)
        {
            Console.WriteLine("Who will enter the arena first? *Enter the name of the first pokemon*");
            string name_pokemon = Console.ReadLine();
            Pokemon pokemon_first = pokemons.Find(p => p.Name == name_pokemon);

            Console.WriteLine("Who will enter the arena second?");
            string second_pokemon = Console.ReadLine();
            Pokemon pokemon_second = pokemons.Find(p => p.Name == second_pokemon);
            bool result = Battle.Fight(pokemon_first, pokemon_second);

            if (result == true)
            {
                Console.WriteLine($"The winner is {pokemon_first.Name}");
                pokemon_first.PrintImage();
            }
            else
            {
                Console.WriteLine($"The winner is {pokemon_second.Name}");
                pokemon_second.PrintImage();
            }
        }
    }
}

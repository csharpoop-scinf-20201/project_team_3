﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Proiect.Interfaces;

namespace Proiect.Classes
{
    class Sandshrew : Pokemon, ISand
    {
        public Sandshrew()
        {
            Name = "Sandshrew";
            HP = 50;
            Attack = 75;
            Defense = 85;
            SpecialAttack = 20;
            SpecialDefense = 30;
            Speed = 40;
            Height = 200;
            Weight = 26.5;
            Category = "Mouse";
            Abilities = "Sand Veil";
            Gender = "male";
            Type = new List<string>() { "Ground" };
            Weaknesses = new List<string>() { "Water", "Grass", "Ice" };
        }
        public Sandshrew(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
           base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return $"{Name} is eating...";
        }

        public override void PrintImage()
        {
            string s = File.ReadAllText("Sandshrew.txt");
            Console.WriteLine(s);
        }

        public override string Speak()
        {
            return "Sands Sands";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Sandslash();
            Console.WriteLine($"{Name} has evolved to Sandslash");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Sandshrew();
            Console.WriteLine("This is the base stage");
        }

        public void Sand_Veil(bool sandstorm)
        {
            if (sandstorm == true)
            {
                SpecialDefense += 50;
                SpecialAttack += 30;
            }
        }
    }
}

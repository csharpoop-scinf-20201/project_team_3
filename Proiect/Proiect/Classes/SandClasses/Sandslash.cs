﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    class Sandslash:Sandshrew
    {
        public Sandslash()
        {
            Name = "Sandslash";
            HP = 75;
            Attack = 100;
            Defense = 110;
            SpecialAttack = 45;
            SpecialDefense = 55;
            Speed = 65;
            Height = 303;
            Weight = 65.0;
            Category = "Mouse";
            Abilities = "Sand Veil";
            Gender = "male";
            Type = new List<string>() { "Ground" };
            Weaknesses = new List<string>() { "Water", "Grass", "Ice" };
        }
        public Sandslash(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
           base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return $"{Name} is eating...";
        }

        public override void PrintImage()
        {
            string s = File.ReadAllText("Sandslash.txt");
            Console.WriteLine(s);
        }

        public override string Speak()
        {
            return "Sands Sands";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Sandslash();
            Console.WriteLine($"{Name} is the most evolved form");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Sandshrew();
            Console.WriteLine($"{Name} has regrased to Sandshrew");
        }

    }
}

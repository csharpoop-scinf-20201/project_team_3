﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Proiect.Classes
{
    class Blastoise:Butterfree
    {

        public Blastoise() : base()
        {
            Name = "Blastoise";
            HP = 50;
            Attack = 55;
            Defense = 65;
            SpecialAttack = 50;
            SpecialDefense = 70;
            Speed = 50;
            Height = 188;
            Weight = 5;
            Category = "Shellfish";
            Abilities = "Torrent";
            Gender = "male";
            Type = new List<string>() { "Water" };
            Weaknesses = new List<string>() { "Grass", "Electric" };
        }

        public Blastoise(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

      
        public override string Eat()
        {
            return "Fooooooodddd";
        }

        public override void PrintImage()
        {
            string path = "blastoise.txt";
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    Console.WriteLine(s);
                }
            }
        }

        public override string Speak()
        {
            return "Blastoiseeeeeeeee!";
        }



        public override string SuperPower()
        {
            return " Blastoise a activat SUPER-POWER";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Caterpie();
            Console.WriteLine($"{Name} has evolved to Caterpie");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Butterfree();
            Console.WriteLine($"{Name} has regressed to Butterfree");
        }




    }
}


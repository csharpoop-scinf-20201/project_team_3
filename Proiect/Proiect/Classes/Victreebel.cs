﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    class Victreebel : Weepinbell
    {
        // Acesta este constructorul implicit si il mosteneste pe cel din clasa de baza
        public Victreebel() : base()
        {
            this.Name = "Victreebel";
            this.HP = 5;
            this.Attack = 7;
            this.Defense = 4;
            this.SpecialAttack = 6;
            this.SpecialDefense = 5;
            this.Speed = 5;
            this.Height = 170.18;
            this.Weight = 15.51;
            this.Category = "Flycatcher";
            this.Abilities = "Chlorophyll";
            this.Gender = "Male";
            this.Type = new List<string>(new string[] { "Grass", "Poison" });
            this.Weaknesses = new List<string>(new string[] { "Fire", "Ice", "Psychic", "Flying" });
        }

        // Acesta este constructorul complet si il mosteneste pe cel din clasa de baza
        public Victreebel(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
            base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return "Yummyyy";
        }

        public override void PrintImage()
        {
            string text = File.ReadAllText("Victreebel.txt");
            Console.WriteLine(text);
        }

        public override string Speak()
        {
            return "Victreebel!";
        }

        // Acesta este abilitatea speciala a lui Oddish. Cand este il lumina soarelui ii creste Speed cu 4
        public override void Chlorophyll(bool sunlight)
        {
            if (sunlight == true)
            {
                Speed += 7;
            }
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Victreebel();
            Console.WriteLine("This is the final stage");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Weepinbell();
            Console.WriteLine($"{Name} has regressed to Weepinbell");
        }
    }
}

﻿using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    class Gloom : Oddish
    {
        // Acesta este constructorul implicit si il mosteneste pe cel din clasa de baza
        public Gloom() : base()
        {
            this.Name = "Gloom";
            this.HP = 4;
            this.Attack = 4;
            this.Defense = 5;
            this.SpecialAttack = 5;
            this.SpecialDefense = 5;
            this.Speed = 3;
            this.Height = 78.74;
            this.Weight = 8.6;
            this.Category = "Weed";
            this.Abilities = "Chlorophyll";
            this.Gender = "Male";
            this.Type = new List<string>(new string[] { "Grass", "Poison" });
            this.Weaknesses = new List<string>(new string[] { "Fire", "Ice", "Psychic", "Flying" });
        }

        // Acesta este constructorul complet si il mosteneste pe cel din clasa de baza
        public Gloom(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) :
            base(Name, HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed, Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

        public override string Eat()
        {
            return "Yummyyy";
        }

        public override void PrintImage()
        {
            string text = File.ReadAllText("Gloom.txt");
            Console.WriteLine(text);
        }

        public override string Speak()
        {
            return "Gloooooom!";
        }

        // Acesta este abilitatea speciala a lui Oddish. Cand este il lumina soarelui ii creste Speed cu 4
        public override void Chlorophyll(bool sunlight)
        {
            if (sunlight == true)
            {
                Speed += 4;
            }
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Vileplume();
            Console.WriteLine($"{Name} has evolved to Vileplume");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Oddish();
            Console.WriteLine($"{Name} has regressed to Oddish");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Proiect.Interfaces;

namespace Proiect.Classes
{
    public abstract class Pokemon
    {
        public static int ID = 0;
        // Fiecare pokemon va avea un ID_particular pentru identificare 
        public int ID_particular { get; set; }
        // Aici se trece Name
        public string Name { get; set; }
        // Aici se trec punctele de HP
        public int HP { get; set; }
        // Aici se trec punctele de Attack
        public int Attack { get; set; }
        // Aici se trec punctele de Defense
        public int Defense { get; set; }
        // Aici se trec punctele de SpecialAttack
        public int SpecialAttack { get; set; }
        // Aici se trec punctele de SpecialDefense
        public int SpecialDefense { get; set; }
        // Aici se trec punctele de Speed
        public int Speed { get; set; }
        // Aici se trece Height
        public double Height { get; set; }
        // Aici se trece Weight
        public double Weight { get; set; }
        // Aici se trece Category
        public string Category { get; set; }
        // Aici se trece Abilities
        public string Abilities { get; set; }
        // Aici se trece Gender
        public string Gender { get; set; }
        // Aici se trece lista de Type
        public List<string> Type { get; set; }
        // Aici se trece lista de Weaknesses
        public List<string> Weaknesses { get; set; }


        public Pokemon()
        {
            ID_particular = ++ID;
        }

        public Pokemon(string Name, int HP, int Attack, int Defense, int SpecialAttack, int SpecialDefense, int Speed, double Height, double Weight, string Category, string Abilities, string Gender, List<string> Type, List<string> Weaknesses) : this()
        {
            this.Name = Name;
            this.HP = HP;
            this.Attack = Attack;
            this.Defense = Defense;
            this.SpecialAttack = SpecialAttack;
            this.SpecialDefense = SpecialDefense;
            this.Speed = Speed;
            this.Height = Height;
            this.Weight = Weight;
            this.Category = Category;
            this.Abilities = Abilities;
            this.Gender = Gender;
            this.Type = Type;
            this.Weaknesses = Weaknesses;
        }

        // Se transforma o imagine in cod ASCII si se afiseaza in consola.
        public abstract void PrintImage();
        // Scriere "sunet" scos de pokemon in consola (cautati pe youtube)
        public abstract string Speak();
        //Sa scrie ceva in consola cand mananca (returneaza un string)
        public abstract string Eat();


        public void AttackFunction(Pokemon pokemon)
        {
            pokemon.HP = pokemon.HP - (this.Attack / 2 + this.Speed / 2) + (pokemon.Defense / 2);
        }


        public void SpecialAttackFunction(Pokemon pokemon)
        {
            pokemon.HP = pokemon.HP - (this.SpecialAttack / 2 + this.Speed / 2) + (pokemon.SpecialDefense / 2 + pokemon.Speed / 2);
        }

        public abstract void Evolution(ref Pokemon pokemon);
        public abstract void Involution(ref Pokemon pokemon);

        public void Empty()
        {
            this.Name = String.Empty;
            this.HP = 0;
            this.Attack = 0;
            this.Defense = 0;
            this.SpecialAttack = 0;
            this.SpecialDefense = 0;
            this.Speed = 0;
            this.Height = 0;
            this.Weight = 0;
            this.Category = String.Empty;
            this.Abilities = String.Empty;
            this.Gender = String.Empty;
            this.Type.Clear();
            this.Weaknesses.Clear();
        }
        //Displays all the information about a pokemon
        public string Present()
        {
            return $"Name: {Name}\n" +
                $"HP: {HP}\n" +
                $"Attack: {Attack}\n" +
                $"Defense: {Defense}\n" +
                $"Special Attack: {SpecialAttack}\n" +
                $"Special Defence: {SpecialDefense}\n" +
                $"Speed: {Speed}\n" +
                $"Height: {Height}cm\n" +
                $"Weight: {Weight}kg\n" +
                $"Category: {Category}\n" +
                $"Abilities: {Abilities}\n" +
                $"Gender: {Gender}\n" +
                $"Type: {string.Join(", ", Type)}\n" +
                $"Weaknesses: {string.Join(", ", Weaknesses)}\n";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    public static class EvPokemon
    {
        public static void EvolvePoke(List<Pokemon> pokemons)
        {
            Console.WriteLine("Evolve a pokemon");
            string pokename = Console.ReadLine();
            Pokemon poke_name = pokemons.Find(p => p.Name == pokename);
            poke_name.Evolution(ref poke_name);
            Console.WriteLine(poke_name.Speak());
        }

        public static void DevolvePoke(List<Pokemon> pokemons)
        {
            Console.WriteLine("Evolve a pokemon");
            string pokename = Console.ReadLine();
            Pokemon poke_name = pokemons.Find(p => p.Name == pokename);
            poke_name.Involution(ref poke_name);
            Console.WriteLine(poke_name.Speak());
        }
    }
}

﻿using Proiect.Classes.BulbasaursClasses;
using Proiect.Classes.CharmandersClasses;
using Proiect.Classes.Nido;
using Proiect.Classes.PidgeyClasses;
using Proiect.Classes.WeedleClasses;
using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Classes
{
    class Pokedex : IPokedex
    {
        private enum PokeType
        {
            Bug,
            Dark,
            Dragon,
            Electric,
            Fairy,
            Fighting,
            Fire,
            Flying,
            Ghost,
            Grass,
            Ground,
            Ice,
            Normal,
            Poison,
            Psyhic,
            Rock,
            Steel,
            Water
        }

        private enum PokeName
        {
            Bulbasaur,
            Ivysaur,
            Venusaur,
            MegaVenusaur,
            GigantamaxVenusaur,
            Charmander,
            Charizard,
            Charmeleon,
            MegaCharizardX,
            GigantamaxCharizard,
            Nidoqueen,
            Nidoran,
            Nidorina,
            Pichu,
            Pikachu,
            Raichu,
            Sandshrew,
            Sandslash,
            Blastoise,
            Butterfree,
            Caterpie,
            Metapod,
            Squirtle,
            Wartortle,
            Bellossom,
            Bellsprout,
            Gloom,
            Oddish,
            Victreebel,
            Vileplume,
            Weepinbell,
            Pidgeot,
            Pidgeotto,
            Pidgey,
            Beedrill,
            Kakuna,
            Weedle
        }
        private List<Pokemon> Pokemons { get; set; } = new List<Pokemon>();

        public Pokedex()
        {
            Pokemons.AddRange(new List<Pokemon>
            {
                new Bulbasaur(),
                new Ivysaur(),
                new Venusaur(),
                new MegaVenusaur(),
                new GigantamaxVenusaur(),
                new Charmander(),
                new Charmeleon(),
                new Charizard(),
                new MegaCharizardX(),
                new GigantamaxCharizard(),
                new Nidoqueen(),
                new Nidoran(),
                new Nidorina(),
                new Pichu(),
                new Pikachu(),
                new Raichu(),
                new Sandshrew(),
                new Sandslash(),
                new Blastoise(),
                new Butterfree(),
                new Caterpie(),
                new Metapod(),
                new Squirtle(),
                new Wartortle(),
                new Bellossom(),
                new Bellsprout(),
                new Gloom(),
                new Oddish(),
                new Victreebel(),
                new Vileplume(),
                new Weepinbell(),
                new Pidgeot(),
                new Pidgeotto(),
                new Pidgey(),
                new Beedrill(),
                new Kakuna(),
                new Weedle(),

            });
        }

        public void Empty()
        {
            Pokemons.Clear();
        }
        
        public List<Pokemon> GetPokemons()
        {
            return Pokemons;
        }
        
        public void DisplayAll()
        {
            foreach (var pokemon in Pokemons)
                Console.WriteLine(pokemon.Present());
        }        
        
        public void DisplayByType(string type)
        {
            if (Enum.IsDefined(typeof(PokeType), type))
            {
                foreach (var pokemon in Pokemons)
                {
                    if (pokemon.Type.Contains(type))
                        Console.WriteLine(pokemon.Present());
                }
            }
            else
                Console.WriteLine("The type you entered is invalid!");
        }

        public void DisplayByName(string name)
        {
            if (Enum.IsDefined(typeof(PokeName), name))
            {
                foreach (var pokemon in Pokemons)
                {
                    if (pokemon.Name.Equals(name))
                    {
                        pokemon.Present();
                        break;
                    }
                }
            }
            else
                Console.WriteLine("The name you entered is invalid!");
        }
        public void PrintAll()
        {
            foreach (var pokemon in Pokemons)
                pokemon.PrintImage();
        }

        public void PrintByType(string type)
        {
            if (Enum.IsDefined(typeof(PokeType), type))
            {
                foreach (var pokemon in Pokemons)
                {
                    if (pokemon.Type.Contains(type))
                        pokemon.PrintImage();
                }
            }
            else
                Console.WriteLine("The type you entered is invalid!");
        }

        public void PrintByName(string name)
        {
            if (Enum.IsDefined(typeof(PokeName), name))
            {
                foreach (var pokemon in Pokemons)
                {
                    if (pokemon.Name.Equals(name))
                    {
                        pokemon.PrintImage();
                        break;
                    }
                }
            }
            else
                Console.WriteLine("The name you entered is invalid!");
        }
    }
}

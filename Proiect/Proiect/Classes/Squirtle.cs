﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Proiect.Classes
{
    class Squirtle: Metapod
    {

        public Squirtle() : base()
        {
            Name = "Squirtle";
            HP = 30;
            Attack = 35;
            Defense = 40;
            SpecialAttack = 30;
            SpecialDefense = 40;
            Speed = 30;
            Height = 10;
            Weight = 30;
            Category = "Tiny Turtle";
            Abilities = "Torrent";
            Gender = "male";
            Type = new List<string>() { "Water" };
            Weaknesses = new List<string>() { "Grass", "Rock" };
        }

        public Squirtle(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

       

        public override string Eat()
        {
            return "Fooooooodddd";
        }

        public override void PrintImage()
        {
            string path = "squirtle.txt";
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    Console.WriteLine(s);
                }
            }
        }

        public override string Speak()
        {
            return "Squirtleeeeeeeeeeee!";
        }

       

        public override string SuperPower()
        {
            return " Squirtle a activat SUPER-POWER";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Wartortle();
            Console.WriteLine($"{Name} has evolved to Wartortle");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Metapod();
            Console.WriteLine($"{Name} has regressed to Metapod");
        }
    }
}
   
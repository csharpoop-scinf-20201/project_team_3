﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Proiect.Classes
{
    class Caterpie:Blastoise
    {
        public Caterpie() : base()
        {
            Name = "Caterpie";
            HP = 30;
            Attack = 25;
            Defense = 35;
            SpecialAttack = 20;
            SpecialDefense = 20;
            Speed = 30;
            Height = 10;
            Weight = 6;
            Category = "Worm";
            Abilities = "Shield Dust";
            Gender = "male";
            Type = new List<string>() { "Bug" };
            Weaknesses = new List<string>() { "Fire", "Rock", "Flying" };
        }

        public Caterpie(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

       

        public override string Eat()
        {
            return "Fooooooodddd";
        }

        public override void PrintImage()
        {
            string path = "caterpie.txt";
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    Console.WriteLine(s);
                }
            }
        }

        public override string Speak()
        {
            return "Cateerpieeeee!";
        }

        

        public override string SuperPower()
        {
            return " Caterpie a activat SUPER-POWER";
        }

        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Metapod();
            Console.WriteLine($"{Name} has evolved to Metapod");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Blastoise();
            Console.WriteLine($"{Name} has regressed to Blastoise");
        }

    }
}

   
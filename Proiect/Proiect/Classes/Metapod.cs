﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Proiect.Classes
{
    class Metapod:Caterpie
    {

        public Metapod() : base()
        {
            Name = "Metapod";
            HP = 30;
            Attack = 25;
            Defense = 40;
            SpecialAttack = 20;
            SpecialDefense = 20;
            Speed = 20;
            Height = 20;
            Weight = 21;
            Category = "Cocoon";
            Abilities = "Shed Skin";
            Gender = "male";
            Type = new List<string>() { "Bug" };
            Weaknesses = new List<string>() { "Fire", "Rock", "Flying" };
        }

        public Metapod(string Name, int HP, int Attack, int Defense, int SpecialAttack,
            int SpecialDefense, int Speed, double Height, double Weight,
            string Category, string Abilities, string Gender,
            List<string> Type, List<string> Weaknesses) : base(Name,
                HP, Attack, Defense, SpecialAttack, SpecialDefense, Speed,
                Height, Weight, Category, Abilities, Gender, Type, Weaknesses)
        {

        }

       
        public override string Eat()
        {
            return "Fooooooodddd";
        }

        public override void PrintImage()
        {
            string path = "metapod.txt";
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    Console.WriteLine(s);
                }
            }
        }

        public override string Speak()
        {
            return "Metapooooood!";
        }

       

        public override string SuperPower()
        {
            return " Metapod a activat SUPER-POWER";
        }


        public override void Evolution(ref Pokemon pokemon)
        {
            pokemon = new Squirtle();
            Console.WriteLine($"{Name} has evolved to Squirtle");
        }

        public override void Involution(ref Pokemon pokemon)
        {
            pokemon = new Caterpie();
            Console.WriteLine($"{Name} has regressed to Caterpie");
        }


    }
}
    
﻿using Proiect.Classes;
using Proiect.Classes.BulbasaursClasses;
using Proiect.Classes.CharmandersClasses;
using Proiect.Classes.Nido;
using Proiect.Classes.PidgeyClasses;
using Proiect.Classes.WeedleClasses;
using Proiect.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect
{
    
    sealed public class World : IWorld
    {
        public IPokedex pokedex = new Pokedex();

        public void Init()
        {

            var arr = new[]
               {
        @"     _____      _                                  ",
        @"    |  __  \   | |                                 ",
        @"    | |__) |__ | | _____ _ __ ___   ___  _ __      ",
        @"    | ___ / _ \| |/ / _ \ '_ ` _ \ / _ \| '_ \     ",
        @"    | |  | (_) |   < __ / | | | | | (_) | | | |    ",
        @"    |_|  \___ /|_|\_\___|_| |_| |_|\___/|_| |_|    ",

        };

            Console.WriteLine("\n");
            foreach (string line in arr)
            {
                Console.WriteLine(line);
            }

            Console.WriteLine("\nWelcome to Pokemon World!\n");

            while (true)
            {
                Console.WriteLine("For more information type help/ for exit type quit");
                string command = Console.ReadLine();

                if (command == "quit")
                {
                    break;
                }
                if (command == "help")
                {
                    Console.WriteLine("\n 1.Presentation");
                    Console.WriteLine(" 2.Fight mode");
                    Console.WriteLine(" 3.Print pokemon image");
                    Console.WriteLine(" 4.What the pokemon eats?");
                    Console.WriteLine(" 5.What sounds does the pokemon make?");
                    Console.WriteLine(" 6.Evolve the pokemon");
                    Console.WriteLine(" 7.De-evolve the pokemon");
                    Console.WriteLine(" 8.Create a new pokemon");
                    Console.WriteLine(" 9.SuperPower mode");
                    Console.WriteLine(" 10.Quit");



                    HelpMenu();
                }
                else
                {
                    Console.WriteLine("\nIncorrect command");
                }
            }
        }

        //private void ResultBattle(ref Pokemon p1, ref Pokemon p2)
        //{
        //    var battle = new World();
        //    bool result = battle.Fight(p1, p2);
        //    if (result == true)
        //    {
        //        p1.Evolution(ref p1);
        //        p2.Involution(ref p2);
        //    }
        //    else
        //    {
        //        p2.Evolution(ref p2);
        //        p1.Involution(ref p1);
        //    }
        //}


        //private bool Fight(Pokemon poke1, Pokemon poke2)
        //{
        //    while (poke1.HP > 0 || poke2.HP > 0)
        //    {
        //        poke1.AttackFunction(poke2);
        //        Log(poke1, poke2);
        //        if (poke2.HP <= 0)
        //        {
        //            Console.WriteLine($"{poke1.Name} wins!");
        //            return true;
        //        }

        //        poke2.AttackFunction(poke1);
        //        Log(poke1, poke2);
        //        if (poke1.HP <= 0)
        //        {
        //            Console.WriteLine($"{poke2.Name} wins!");
        //            return false;
        //        }

        //        poke1.SpecialAttackFunction(poke2);
        //        Log(poke1, poke2);
        //        if (poke2.HP <= 0)
        //        {
        //            Console.WriteLine($"{poke1.Name} wins!");
        //            return true;
        //        }

        //        poke2.SpecialAttackFunction(poke1);
        //        Log(poke1, poke2);
        //        if (poke1.HP <= 0)
        //        {
        //            Console.WriteLine($"{poke2.Name} wins!");
        //            return false;
        //        }
        //    }
        //    return false;
        //}

        //private void Log(Pokemon pokemon, Pokemon pokemon2)
        //{
        //    Console.WriteLine($"{pokemon2.Name} -> {pokemon2.HP}");
        //    Console.WriteLine($"{pokemon.Name} -> {pokemon.HP}");
        //}

        public void DisplayPokemon(string input)
        {
            if (input.Equals("all", StringComparison.OrdinalIgnoreCase))
                pokedex.DisplayAll();
            else if (input.Equals("type", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Write a type of pokemons");
                input = Console.ReadLine();

                pokedex.DisplayByType(input);
            }
            else
            {
                Console.WriteLine("Write the name of a pokemon");
                input = Console.ReadLine();

                pokedex.DisplayByName(input);
            }
        }
        public void DisplayImage(string input)
        {
            if (input.Equals("all", StringComparison.OrdinalIgnoreCase))
                pokedex.PrintAll();
            else if (input.Equals("type", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Write a type of pokemons");
                input = Console.ReadLine();

                pokedex.PrintByType(input);
            }
            else
            {
                Console.WriteLine("Write the name of a pokemon");
                input = Console.ReadLine();

                pokedex.PrintByName(input);
            }
        }

        public void HelpMenu()
        {

            string helpCommand = Console.ReadLine();

            if (helpCommand == "1")
            {
                Console.WriteLine("Write a type of pokemons or all to display all");
                helpCommand = Console.ReadLine();

                DisplayPokemon(helpCommand);

            }
            if (helpCommand == "2")
            {
                Pokemon_Fight fight = new Pokemon_Fight();
                fight.Arena(pokedex.GetPokemons());

            }
            if (helpCommand == "3")
            {
                Console.WriteLine("You can print ASCII images of all pokemons, a type of pokemons or just a pokemon");
                helpCommand = Console.ReadLine();

                DisplayImage(helpCommand);
            }
            if (helpCommand == "4")
            {
                Eat.Feed(pokedex.GetPokemons());
            }
            if (helpCommand == "5")
            {
                foreach(var poke in pokedex.GetPokemons())
                {
                    Console.WriteLine($"{poke.Name} - > {poke.Speak()}");
                }
            }
            if (helpCommand == "6")
            {
                EvPokemon.EvolvePoke(pokedex.GetPokemons());
            }
            if (helpCommand == "7")
            {
                EvPokemon.DevolvePoke(pokedex.GetPokemons());
            }
            if (helpCommand == "8")
            {
                // de implementat lista cu toate tipurile de pokemon , in functie de cel ales sa creez pokemonul
                List<string> pokemonTypesList = new List<string>();

                string pokemontype1 = "1.Bulbasaur";
                pokemonTypesList.Add(pokemontype1);

                string pokemontype2 = "2.Ivysaur";
                pokemonTypesList.Add(pokemontype2);

                string pokemontype3 = "3.Venusaur";
                pokemonTypesList.Add(pokemontype3);

                string pokemontype4 = "4.MegaVenusaur";
                pokemonTypesList.Add(pokemontype4);

                string pokemontype5 = "5.GigantamaxVenusaur";
                pokemonTypesList.Add(pokemontype5);

                string pokemontype6 = "6.Charmander";
                pokemonTypesList.Add(pokemontype6);

                string pokemontype7 = "7.Charizard";
                pokemonTypesList.Add(pokemontype7);

                string pokemontype8 = "8.Charmeleon";
                pokemonTypesList.Add(pokemontype8);

                string pokemontype9 = "9.MegaCharizardX";
                pokemonTypesList.Add(pokemontype9);

                string pokemontype10 = "10.GigantamaxCharizard";
                pokemonTypesList.Add(pokemontype10);

                string pokemontype11 = "11.Nidoqueen";
                pokemonTypesList.Add(pokemontype11);

                string pokemontype12 = "12.Nidoran";
                pokemonTypesList.Add(pokemontype12);

                string pokemontype13 = "13.Nidorina";
                pokemonTypesList.Add(pokemontype13);

                string pokemontype14 = "14.Pichu";
                pokemonTypesList.Add(pokemontype14);

                string pokemontype15 = "15.Pikachu";
                pokemonTypesList.Add(pokemontype15);

                string pokemontype16 = "16.Raichu";
                pokemonTypesList.Add(pokemontype16);

                string pokemontype17 = "17.Sandshrew";
                pokemonTypesList.Add(pokemontype17);

                string pokemontype18 = "18.Sandslash";
                pokemonTypesList.Add(pokemontype18);

                string pokemontype19 = "19.Blastoise";
                pokemonTypesList.Add(pokemontype19);

                string pokemontype20 = "20.Butterfree";
                pokemonTypesList.Add(pokemontype20);

                string pokemontype21 = "21.Caterpie";
                pokemonTypesList.Add(pokemontype21);

                string pokemontype22 = "22.Metapod";
                pokemonTypesList.Add(pokemontype22);

                string pokemontype23 = "23.Squirtle";
                pokemonTypesList.Add(pokemontype23);

                string pokemontype24 = "24.Wartortle";
                pokemonTypesList.Add(pokemontype24);

                string pokemontype25 = "25.Bellossom";
                pokemonTypesList.Add(pokemontype25);

                string pokemontype26 = "26.Bellsprout";
                pokemonTypesList.Add(pokemontype26);

                string pokemontype27 = "27.Gloom";
                pokemonTypesList.Add(pokemontype27);

                string pokemontype28 = "28.Oddish";
                pokemonTypesList.Add(pokemontype28);

                string pokemontype29 = "29.Victreebel";
                pokemonTypesList.Add(pokemontype29);

                string pokemontype30 = "30.Vileplume";
                pokemonTypesList.Add(pokemontype30);

                string pokemontype31 = "31.Weepinbell";
                pokemonTypesList.Add(pokemontype31);

                string pokemontype32 = "32.Pidgeot";
                pokemonTypesList.Add(pokemontype32);

                string pokemontype33 = "33.Pidgeotto";
                pokemonTypesList.Add(pokemontype33);

                string pokemontype34 = "34.Pidgey";
                pokemonTypesList.Add(pokemontype34);

                string pokemontype35 = "35.Beedrill";
                pokemonTypesList.Add(pokemontype35);

                string pokemontype36 = "36.Kakuna";
                pokemonTypesList.Add(pokemontype36);

                string pokemontype37 = "37.Weedle";
                pokemonTypesList.Add(pokemontype37);

                foreach (var item in pokemonTypesList)
                {
                    Console.WriteLine(item);
                }

                Console.WriteLine("Select what type of pokemon you want to create : ");
                int optiuneaSelectata = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter the name");
                string name = Console.ReadLine();

                Console.WriteLine("Enter the HP");
                int hp = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter the attack");
                int attack = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter the defense");
                int defense = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter the special attack");
                int specialAttack = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter the special defense");
                int specialDefense = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter the speed");
                int speed = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter the height");
                double height = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Enter the weight");
                double weight = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Enter the category name");
                string category = Console.ReadLine();

                Console.WriteLine("Enter the ability");
                string ability = Console.ReadLine();

                Console.WriteLine("Enter the gender");
                string gender = Console.ReadLine();

                Console.WriteLine("Enter the pokemon type");
                List<string> typeList = new List<string>();
                string type = Console.ReadLine();

                while (true)
                {
                    type = Console.ReadLine();
                    if (type.Equals("q"))
                    {
                        break;
                    }
                    typeList.Add(type);
                }

                Console.WriteLine("Enter the pokemon weaknesses");
                List<string> weaknessesList = new List<string>();
                string weaknesses = Console.ReadLine();

                while (true)
                {
                    weaknesses = Console.ReadLine();
                    if (weaknesses.Equals("q"))
                    {
                        break;
                    }
                    weaknessesList.Add(weaknesses);
                }

                switch (optiuneaSelectata)
                {
                    case 1:
                        Bulbasaur newPokemon = new Bulbasaur(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 2:
                        Ivysaur newPokemon2 = new Ivysaur(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 3:
                        Venusaur newPokemon3 = new Venusaur(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 4:
                        MegaVenusaur newPokemon4 = new MegaVenusaur(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 5:
                        GigantamaxVenusaur newPokemon5 = new GigantamaxVenusaur(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;
                    case 6:
                        Charmander newPokemon6 = new Charmander(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 7:
                        Charizard newPokemon7 = new Charizard(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 8:
                        Charmeleon newPokemon8 = new Charmeleon(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 9:
                        MegaCharizardX newPokemon9 = new MegaCharizardX(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 10:
                        GigantamaxCharizard newPokemon10 = new GigantamaxCharizard(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 11:
                        Nidoqueen newPokemon11 = new Nidoqueen(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 12:
                        Nidoran newPokemon12 = new Nidoran(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 13:
                        Nidorina newPokemon13 = new Nidorina(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 14:
                        Pichu newPokemon14 = new Pichu(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 15:
                        Pikachu newPokemon15 = new Pikachu(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 16:
                        Raichu newPokemon16 = new Raichu(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 17:
                        Sandshrew newPokemon17 = new Sandshrew(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 18:
                        Sandslash newPokemon18 = new Sandslash(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 19:
                        Blastoise newPokemon19 = new Blastoise(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;
                    case 20:
                        Butterfree newPokemon20 = new Butterfree(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 21:
                        Caterpie newPokemon21 = new Caterpie(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 22:
                        Metapod newPokemon22 = new Metapod(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 23:
                        Squirtle newPokemon23 = new Squirtle(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 24:
                        Wartortle newPokemon24 = new Wartortle(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 25:
                        Bellossom newPokemon25 = new Bellossom(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 26:
                        Bellsprout newPokemon26 = new Bellsprout(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 27:
                        Gloom newPokemon27 = new Gloom(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 28:
                        Oddish newPokemon28 = new Oddish(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 29:
                        Victreebel newPokemon29 = new Victreebel(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 30:
                        Vileplume newPokemon30 = new Vileplume(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 31:
                        Weepinbell newPokemon31 = new Weepinbell(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 32:
                        Pidgeot newPokemon32 = new Pidgeot(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 33:
                        Pidgeotto newPokemon33 = new Pidgeotto(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 34:
                        Pidgey newPokemon34 = new Pidgey(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 35:
                        Beedrill newPokemon35 = new Beedrill(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 36:
                        Kakuna newPokemon36 = new Kakuna(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    case 37:
                        Weedle newPokemon37 = new Weedle(name, hp, attack, defense, specialAttack, specialDefense, speed, height, weight, category, ability, gender, typeList, weaknessesList);
                        break;

                    default:
                        Console.WriteLine("Incorrect command!");
                        break;
                }
            }

            if (helpCommand == "9")
            {
            Console.WriteLine("We dont have super-Powers! ");
            }
        }



    }
}

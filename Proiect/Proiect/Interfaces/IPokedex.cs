﻿using Proiect.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Interfaces
{
    public interface IPokedex
    {
        //Clears pokemon list
        void Empty();

        //Returns a list full of pokemons
        List<Pokemon> GetPokemons();

        //Display all the information about all the pokemons found
        void DisplayAll();

        //Display all the information about pokemons of a given type
        void DisplayByType(string type);

        //Display all the information about a pokemon with given name
        void DisplayByName(string name);

        //Print ASCII images of all pokemons
        void PrintAll();

        //Print ASCII image of pokemons of that type
        void PrintByType(string type);

        //Print ASCII image of pokemon with specific name
        void PrintByName(string name);
    }
}

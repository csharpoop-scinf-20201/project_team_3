﻿using Proiect.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Interfaces
{
    interface IPokemon
    {
        void PrintImage();
        // Scriere "sunet" scos de pokemon in consola (cautati pe youtube)
        string Speak();
        //Sa scrie ceva in consola cand mananca (returneaza un string)
        string Eat();


        void AttackFunction(Pokemon pokemon);


        void SpecialAttackFunction(Pokemon pokemon);

        void Evolution(ref Pokemon pokemon);
        void Involution(ref Pokemon pokemon);
        void Empty();
    }
}

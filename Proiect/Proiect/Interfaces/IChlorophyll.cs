﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect.Interfaces
{
    interface IChlorophyll : IPokemon
    {
        void Chlorophyll(bool sunlight);
    }
}
